import java.math.BigInteger;

public class driver {
    /*
     * https://leetcode.com/problems/champagne-tower/
     * */
    public static double champagneTower(int poured, int query_row, int query_glass) {
        double[][] A = new double[102][102];
        A[0][0] = (double) poured;
        for (int rows = 0; rows <= query_row; ++rows) {
            for (int column = 0; column <= rows; ++column) {
                double q = (A[rows][column] - 1.0) / 2.0;
                if (q > 0) {
                    A[rows+1][column] += q;
                    A[rows+1][column+1] += q;
                }
            }
        }

        return Math.min(1, A[query_row][query_glass]);
    }


    public static boolean isSelfDescribingNumber(long number) {
        // count each digit, 10 digits 0-9
        String sNum = Long.toString(number);
        int[] count = new int[9];
        for(int i = 0; i < sNum.length(); i++){
            count[Integer.parseInt(sNum.charAt(i) + "")]++;
        }

        // check each index with occurrence
        for(int j = 0; j < sNum.length(); j++){
            int occurrencesMatch = Integer.parseInt(sNum.charAt(j) + "");
            System.out.println(j + " " + count[j]);
            if(count[j] != occurrencesMatch){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        System.out.println(isSelfDescribingNumber(42101000));
    }
}
