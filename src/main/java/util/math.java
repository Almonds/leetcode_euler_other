package util;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class math {
    public static long fib(int num, HashMap<Integer, Long> memo){
        if(memo.containsKey(num)) return memo.get(num);
        if(num <= 2) return 1;
        long fibTotal = fib(num - 2, memo) + fib(num - 1, memo);
        memo.put(num, fibTotal);
        return fibTotal;
    }

    public static <T> void printList(List<T> list) {
        for(T t : list){
            System.out.print(t + " ");
        }
        System.out.println();
    }

    // can reuse elements
    public static List<Integer> shortestArrayOfTargetSum(int[] sourceArray, int targetSum, HashMap<Integer, List<Integer>> steps) {
        if(targetSum == 0) return new LinkedList<>();
        if(targetSum < 0) return null;
        if(steps.containsKey(targetSum)) {
            return steps.get(targetSum);
        }

        List<Integer> shortestList = null;
        for(Integer i : sourceArray){
            List<Integer> remainder = shortestArrayOfTargetSum(sourceArray, targetSum - i, steps);
            if (remainder != null) {
                remainder.add(i);
                steps.put(targetSum - i, remainder);
                if(shortestList == null || remainder.size() < shortestList.size()){
                    shortestList = remainder;
                }
            }
        }
        return shortestList;
    }

    public static void main(String[] args){
//        System.out.println(fib(50, new HashMap<Integer, Long>()));

        int[] array = new int[]{4,3,1,6,3,12,9};
        System.out.println(shortestArrayOfTargetSum(array, 13, new HashMap<Integer, List<Integer>>()).size());
        System.out.println(shortestArrayOfTargetSum(array, 7, new HashMap<Integer, List<Integer>>()).size());
        System.out.println(shortestArrayOfTargetSum(array, 9, new HashMap<Integer, List<Integer>>()).size());
        System.out.println(shortestArrayOfTargetSum(array, 5876, new HashMap<Integer, List<Integer>>()).size());
    }
}
