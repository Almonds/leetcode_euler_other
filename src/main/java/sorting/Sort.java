package sorting;

public class Sort {
    private <T> void swap(T[] data, int i1, int i2){
        T temp = data[i1];
        data[i1] = data[i2];
        data[i2] = temp;
    }

    private static <T extends Comparable<T>> int insertionPosition(T[] array, int start, int end, T key) {
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (key.compareTo(array[mid]) > 0) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return start;
    }
    public static <T extends Comparable<T>> void binaryInsertionSort(T[] data){
        for(int i = 1; i < data.length; ++i) {
            T currentKey = data[i];
            int insertionIndex = insertionPosition(data, 0, i-1, currentKey);
            for (int j = i - 1; j >= insertionIndex; --j) {
                data[j + 1] = data[j];
            }
            data[insertionIndex] = currentKey;
        }
    }



    public static <T extends  Comparable<T>> void minHeapSort(T[] data){

    }

    public static void main(String[]  args){
        String[] t = {"house", "blouse", "douse", "aouse", "faust"};
        binaryInsertionSort(t);
        for(String value : t){
            System.out.println(value);
        }
    }
}
