package hackerrank;

public class SherlockAndAnagrams {

    private static Integer process(String target, String s, Integer matched){
        if(s.isEmpty()) return matched;
        if(target.length() > s.length()) return matched;

        if(s.startsWith(target)) {
            matched++;
        }
        return process(target, s.substring(1), matched);
    }

    public static int sherlockAndAnagrams(String s) {
        int totalAnagrams = 0;
//        totalAnagrams += process("sta", s, 0);

        // start with 1 space then 2 then 3 ...
        for(int i = 1; i < s.length()/2+1; i++){
            for(int j = 0; j < s.length(); j += i){
                if(j+i > s.length()) break;
                String target = s.substring(j,j+i);
                String source = s.substring(j+1);
                System.out.println(target);
//                totalAnagrams += process(target, source, 0);
            }
        }
        return totalAnagrams;
    }

    public static void main(String[] args){
        String test1 = "ifailuhkqq";
        System.out.println(sherlockAndAnagrams("ifailuhkqq"));
    }
}
