package hackerrank;
//https://www.hackerrank.com/challenges/java-comparator/problem
/*
Order by score descending. Same score, alphabetical by name
 */
import java.util.Arrays;
import java.util.Comparator;

public class Checker implements Comparator {
    static class Player {
        String name;
        int score;

        public Player(String n, int s){
            name = n;
            score = s;
        }
    }

    @Override
    public int compare(Object o1, Object o2) {
        Player p1 = (Player) o1;
        Player p2 = (Player) o2;
        int scoreDifference = p2.score - p1.score;
        if(scoreDifference == 0) {
            scoreDifference += p1.name.compareTo(p2.name);
        }
        return scoreDifference;
    }

    public static void main(String[] args){
        Player[] players = new Player[10];
        Checker checker = new Checker();
        players[0] = new Player("aber", 5);
        players[1] = new Player("fetr", 12);
        players[2] = new Player("jfkf", 7);
        players[3] = new Player("vsdd", 7);
        players[4] = new Player("wqer", 12);
        players[5] = new Player("yiho", 2);
        players[6] = new Player("vcxv", 8);
        players[7] = new Player("yure", 15);
        players[8] = new Player("iovv", 11);
        players[9] = new Player("povv", 3);
        Arrays.sort(players, checker);
        for(Player player: players){
            System.out.println(player.name + ": " + player.score);
        }
    }
}
