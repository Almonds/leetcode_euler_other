package hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QueryWithFixedLength {
    public static List<Integer> solve(List<Integer> arr, List<Integer> queries) {
        List<Integer> results = new ArrayList<>();
        for(Integer arraySize: queries){
            int max = Collections.max(arr.subList(0, arraySize));
            int min = max;
            for(int i = arraySize; i < arr.size(); i++){
                if(arr.get(i - arraySize) == max){
                    max = Collections.max(arr.subList(i-arraySize+1, i+1));
                } else if(arr.get(i) > max){
                    max = arr.get(i);
                }
                if(max < min){
                    min = max;
                }
            }
            results.add(min);
        }
        return results;
    }

    public static void main(String[] args) {
        List<Integer> queries = new ArrayList<>(List.of(2, 5, 3, 1));
        List<Integer> array = new ArrayList<>(List.of(33, 11, 44, 11, 55));
        List<Integer> solutions = solve(array, queries);
        for(Integer a : solutions){
            System.out.println(a);
        }
    }
}
