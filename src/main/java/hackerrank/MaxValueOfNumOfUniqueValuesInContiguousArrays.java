package hackerrank;
// https://www.hackerrank.com/challenges/java-dequeue/problem?h_r=profile
import java.util.*;

public class MaxValueOfNumOfUniqueValuesInContiguousArrays {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque<Integer> deque = new ArrayDeque<>();
        int n = in.nextInt();
        int m = in.nextInt();
        HashSet<Integer> set = new HashSet<>();
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            int num = in.nextInt();
            deque.add(num);
            set.add(num);

            if(deque.size() == m){
                if (set.size() > max) {
                    max = set.size();
                }
                Integer first = deque.remove();
                if(!deque.contains(first)){
                    set.remove(first);
                }
            }
        }
        System.out.println(max);
    }
}
