package hackerrank;
// https://www.hackerrank.com/challenges/java-priority-queue/problem?h_r=profile
import java.util.*;

public class StudentPriority {
    static class Student implements Comparable<Student>, Comparator<Student> {
        private int id;
        private String name;
        private double cgpa;

        public Student(){}

        public Student(String name, double cgpa, int id){
            this.id = id;
            this.name = name;
            this.cgpa = cgpa;
        }

        public int getID() {
            return id;
        }

        public String getName() {
            return name;
        }

        public double getCGPA() {
            return cgpa;
        }

        public void setID(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setCGPA(double cgpa) {
            this.cgpa = cgpa;
        }

        @Override
        public int compare(Student s1, Student s2) {
            if(s1.cgpa > s2.cgpa) {
                return -1;
            } else if(s1.cgpa < s2.cgpa) {
                return 1;
            } else if(s1.cgpa == s2.cgpa){
                if(s1.name.compareTo(s2.name) == 0){
                    return Integer.compare(s1.id, s2.id);
                } else {
                    return s1.name.compareTo(s2.name);
                }
            }
            return s1.name.compareTo(s2.name);
        }

        @Override
        public int compareTo(Student student) {
            if(this.cgpa > student.cgpa) {
                return -1;
            } else if(this.cgpa < student.cgpa) {
                return 1;
            } else {
                if(this.name.compareTo(student.name) == 0){
                    return Integer.compare(this.id, student.id);
                } else {
                    return this.name.compareTo(student.name);
                }
            }
        }
    }
    static class Priorities {
        public List<Student> getStudents(List<String> events) {
            List<Student> order = new ArrayList<>();
            for(String event : events) {
                String[] split = event.split(" ");
                if(split[0].equals("ENTER")){
                    Student student = new Student(split[1], Double.parseDouble(split[2]), Integer.parseInt(split[3]));
                    order.add(student);
                } else if(split[0].equals("SERVED")){
                    Collections.sort(order);
                    if(!order.isEmpty()){
                        order.remove(0);
                    }
                }
            }
            return order;
        }
    }

    public static void main(String[] args) {
        List<String> events = new ArrayList<>();
        events.add("ENTER John 3.75 50");
        events.add("ENTER Mark 3.8 24");
        events.add("ENTER Shafaet 3.7 35");
        events.add("SERVED");
        events.add("SERVED");
        events.add("ENTER Samiha 3.85 36");
        events.add("SERVED");
        events.add("ENTER Ashley 3.9 42");
        events.add("ENTER Maria 3.6 46");
        events.add("ENTER Anik 3.95 49");
        events.add("ENTER Dan 3.95 50");
        events.add("SERVED");
        Priorities p = new Priorities();
        List<Student> students = p.getStudents(events);
        for(Student s : students){
            System.out.println(s.getName());
        }
//        PriorityQueue<Student> a = new PriorityQueue<>(new Student());
//        a.add(new Student("A", 3.77, 53));
//        a.add(new Student("A", 3.77, 52));
//        a.add(new Student("A", 3.77, 51));
//        while(!a.isEmpty()){
//            Student student = a.remove();
//            System.out.println(student.id);
//        }
    }
}
