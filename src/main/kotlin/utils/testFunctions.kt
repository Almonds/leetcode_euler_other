package utils
import dataStructures.DoublyLinkedList
import leetCode.*
import leetCode.LeetCodeSolutions.mergeKLists
import leetCode.LeetCodeSolutions.searchMatrix
import leetCode.LeetCodeSolutions.shortestSubArray
import utils.math.mul
import utils.math.pow
import utils.math.sum
import utils.math.maxOf
import utils.math.minOf
import java.util.*

/*
* Intent here is to first test out the functionality of various classes
* and functions I create with simple function calls and inspecting the
* printed values for correctness
*
* Eventually move this to learning to write unit test with JUnit
*/

fun <E : Comparable<E>> printList(list : DoublyLinkedList<E>){
    print("List: ")
    for(i in list)
        print("$i, ")
    println("\nSize: ${list.size()}")
}

fun testLinkedList(){
    val testList : DoublyLinkedList<Int> = DoublyLinkedList<Int>()
    println("Create List With Initial Node of 0")
    testList.addTail(0)
    printList(testList)

    println("-----------------------Test Add Head-----------------------")
    for(i in 1..10)
        testList.addHead(-i)
    printList(testList)

    println("-----------------------Test Add Tail-----------------------")
    for(i in 1..10)
        testList.addTail(i)
    printList(testList)

    println("-----------------------Test Add Index-----------------------")
    println("Add 22 at index 5")
    testList.addIndex(22, 5)
    printList(testList)

    println("\nAdd 22 at index 8")
    testList.addIndex(22, 8)
    printList(testList)

    println("\nAdd 22 at index 2")
    testList.addIndex(22, 2)
    printList(testList)

    println("\nAdd 22 at index 0")
    testList.addIndex(22, 0)
    printList(testList)

    println("\nAdd 22 at index 24")
    testList.addIndex(22, testList.size())
    printList(testList)

    println("\nAdd 22 at index testlist.size+1")
    var retVal = testList.addIndex(24, testList.size()+1)
    if(retVal == null){
        println("pass")
    }
    println("Add 22 at index -1"); testList.addIndex(23, -1)
    retVal = testList.addIndex(22, testList.size()+1)
    if(retVal == null){
        println("pass")
    }
    printList(testList)

    println("-----------------------Test Remove Index-----------------------")

    println("-----------------------Test Remove Head-----------------------")

    println("-----------------------Test Remove Tails-----------------------")

    println("-----------------------Test getItemAtIndex(index) Tails-----------------------")

}

fun testMathFunctions(){
    println("-----------------------Test Math Functions-----------------------")
    println("5+5 = ${sum(5, 5)}")
    println("5*5 = ${mul(5, 5)}")
    println("5^5 = ${pow(5,5)}")
    println("maxOf(10,2) = ${maxOf(10,2)}")
    println("minOf(10,2) = ${minOf(10,2)}\n")
}

fun testLeetCode(){
//    val l1 = DoublyLinkedList<Int>()
//    val l2 = DoublyLinkedList<Int>()
//    l1.addHead(1)
//    l1.addHead(9)
//    l1.addHead(0)
//    l1.addHead(1)
//    l2.addHead(3)
//    l2.addHead(5)
//    l2.addHead(9)
//
//    val retAdd = LeetCodeSolutions.addTwoNumbers(l1, l2)
//    printList(retAdd)
//
//    val retSubCat = LeetCodeSolutions.substringOfConcatedWords("somethingbowlowmowhouselowbowmow", arrayOf("low", "bow", "mow"))
//    print("Substring Concated Words: ")
//    for(i in retSubCat) print("$i ")
//    println()
//
//    //    substringOfConcatedWords("somethingbowlowmowhouselowbowmow", arrayOf("low", "bow", "mow"))
//    val holder = LinkedList<Int>()
//    val rand = Random()
//    for(i in 0..49999){
//        holder.push(rand.nextInt(61) - 30)
//    }
//
//    shortestSubArray(holder.toIntArray(), 25)
//
//    val a = DoublyLinkedList<Int>()
//    val b = DoublyLinkedList<Int>()
//    val c = DoublyLinkedList<Int>()
//    val d = DoublyLinkedList<Int>()
//    val e = DoublyLinkedList<Int>()
//    val f = DoublyLinkedList<Int>()
//    for(i in 0..500){
//        val a1 = rand.nextInt(100)
//        val b1 = rand.nextInt(100)
//        val c1 = rand.nextInt(100)
//        val d1 = rand.nextInt(100)
//        val e1 = rand.nextInt(100)
//        val f1 = rand.nextInt(100)
//        if(a1 > 52) a.addTail(a1)
//        if(b1 > 65) b.addTail(b1)
//        if(c1 > 73) c.addTail(c1)
//        if(d1 > 84) d.addTail(d1)
//        if(e1 > 55) e.addTail(e1)
//        if(f1 > 49) f.addTail(f1)
//    }
//    println("${a.size} ${b.size} ${c.size} ${d.size} ${e.size} ${f.size}")
//    val sortedList = mergeKLists(arrayOf(a,b,c,d,e,f))
//    for(k in sortedList){
//        print("$k ")
//    }
//    println()

    val matrix = arrayOf(
            intArrayOf(4,6,1,9,4),
            intArrayOf(5,6,13,4,7),
            intArrayOf(16,4,7,11,19)
    )
    val tm = Array(9){Array(19){0}}
    searchMatrix(matrix, 8)
}

