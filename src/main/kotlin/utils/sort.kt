package utils

fun bubbleSort(list: Array<Int>): Array<Int>{
    var placesUnsorted = list.size
    var currentPlace: Int
    var smallest: Int
    var smallestIndex: Int
    var temp: Int

    while(placesUnsorted > 0){
        currentPlace = list.size - placesUnsorted
        smallest = list[currentPlace]
        smallestIndex = currentPlace

        for(i in currentPlace..list.size-1){
            if(list[i] < smallest){
                smallest = list[i]
                smallestIndex = i
            }
        }

        temp = list[currentPlace]
        list[currentPlace] = smallest
        list[smallestIndex] = temp
        placesUnsorted--
    }
    return list
}

fun heapSort(list: Array<Int>): Array<Int>{
    TODO()
}

fun quickSort(list: Array<Int>): Array<Int>{
    TODO()
}

