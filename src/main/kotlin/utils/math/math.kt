package utils.math

import kotlin.math.pow

fun sum(a: Int, b: Int) = a + b

fun mul(a: Int, b: Int) = a * b

fun pow(base: Int, power: Int) = base.toDouble().pow(power).toInt()

fun maxOf(a: Int, b: Int) = if(a>b) a else b

fun minOf(a: Int, b: Int) = if(a<b) a else b

fun fibonacci_loop(prev: Int, curr: Int, end: Int){
    println(prev)
    var last = prev
    var now = curr
    var tmp = 0
    var countdown = end

    while(countdown != 0){
        println(now)
        tmp = last
        last = now
        now = tmp + now
        countdown--
    }
}

fun fibonacci_recursive(prev: Int, curr: Int, places: Int){
    println(prev)
    if(places == 0) return
    fibonacci_recursive(curr, curr+prev, places-1)
}

fun reverseNum(x: Int){
    var reversed: Int = 0
    var calc = x

    while(calc != 0){
        reversed = reversed * 10 + calc % 10
        calc = calc / 10
    }
    println(reversed)
}

