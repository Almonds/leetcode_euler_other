package leetCode
import dataStructures.DoublyLinkedList
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors
import java.util.stream.Stream
import kotlin.collections.ArrayList

object LeetCodeSolutions{

// You are given two non-empty linked lists representing two non-negative integers.
// The digits are stored in reverse order and each of their nodes contain a single digit.
// Add the two numbers and return it as a linked list.
// https://leetcode.com/problems/add-two-numbers/description/
fun addTwoNumbers(l1: DoublyLinkedList<Int>, l2: DoublyLinkedList<Int>): DoublyLinkedList<Int> {
    val largest : DoublyLinkedList<Int>
    val smallest : DoublyLinkedList<Int>
    if(l1.size()>l2.size()){
        largest = l1
        smallest = l2
    }
    else{
        largest = l2
        smallest = l1
    }

    var carry  = 0
    val retList = DoublyLinkedList<Int>()
    for(i in 0 until largest.size()){
        val small = if(i < smallest.size()) smallest.getItemAtIndex(i) else 0
        val large = largest.getItemAtIndex(i)
        val total = small!! + large!!
        retList.addTail(total%10 + carry)
        carry = if(total>9) 1 else 0
    }
    return retList
}

// Given two integers dividend and divisor, divide two integers without using multiplication, division and mod operator.
// Return the quotient after dividing dividend by divisor.
// The integer division should truncate toward zero.
// https://leetcode.com/problems/divide-two-integers/
fun divide(dividend: Int, divisor: Int): Int {
    //both are signed then should be positive after division so inverse
    //only one, solution signed
    val sign = (dividend < 0 && divisor > 0) || (dividend > 0 && divisor < 0)
    var absDivident = Math.abs(dividend)
    val absDivisor = Math.abs(divisor)

    var counter = 0
    while(absDivident > 0){
        absDivident -= absDivisor
        if(absDivident >= 0) counter++
    }
    if(sign) counter = -counter

    return counter
}

// You are given a string, s, and a list of words, words, that are all of the same length.
// Find all starting indices of substring(s) in s that is a concatenation of each word in
// words exactly once and without any intervening characters.
// https://leetcode.com/problems/substring-with-concatenation-of-all-words/description/
fun substringOfConcatedWords(s: String, words: Array<String>): List<Int>{
    var sVar = s
    val startIndexes = ArrayList<Int>()
    var offset = 0

    do{
        val pairs = sVar.findAnyOf(words.toMutableList())
        pairs?.let{
            startIndexes.add(pairs.first + offset)
            sVar = sVar.substring(pairs.first + words[0].length)
            offset += pairs.first + words[0].length
        }
    }while(pairs != null)

    return startIndexes
}

/*
Return the length of the shortest, non-empty, contiguous subarray of A with target at least K.
If there is no non-empty subarray with target at least K, return -1.
Example 1:

Input: A = [1], K = 1
Output: 1
Example 2:

Input: A = [1,2], K = 4
Output: -1
Example 3:

Input: A = [2,-1,2], K = 3
Output: 3


Note:

1 <= A.length <= 50000
-10 ^ 5 <= A[i] <= 10 ^ 5
1 <= K <= 10 ^ 9
* */

val lengthList = LinkedList<Int>()

fun shortestSubArray(A: IntArray, K: Int) {
    val sumService = Executors.newFixedThreadPool(8)

    for(i in 0..A.size-1){
        sumService.execute(shor(A, K, i))
    }
    try{
        sumService.shutdown()
        sumService.awaitTermination(20, TimeUnit.SECONDS)
        println(lengthList[0])
    }catch (e: InterruptedException){
        sumService.shutdownNow()
        Thread.currentThread().interrupt()
    }
}
private class shor(A: IntArray, K: Int, i: Int): Runnable{
    val search = A
    val index = i
    val target = K
    override fun run(){
        var length = search[index]
        var i = index
        while(length < target && i < search.size){
            length += search[i]
            i++
        }
        if(length == target) lengthList.add(i)
    }

}

// Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.
// https://leetcode.com/problems/merge-k-sorted-lists/
fun mergeKLists(lists: Array<DoublyLinkedList<Int>>): DoublyLinkedList<Int> {
    val heapSorter = PriorityQueue<Int>()
    for(list in lists){
        heapSorter.addAll(list)
    }

    val retList = DoublyLinkedList<Int>()
    for(item in heapSorter.sorted())
        retList.addTail(item)
    return retList
}



// Given a string s1, we may represent it as a binary tree by partitioning it to two non-empty substrings recursively.
// Below is one possible representation of s1 = "great":
// https://leetcode.com/problems/scramble-string/description/
class TreeString(str: String){
    private class Node(str: String){
        var right: Node? = null
        var left: Node? = null
        val data: String
        init{
            data = str
        }
    }
    private val root: Node
    private val orig: String
    init{
        root = Node(str)
        orig = str
        splitInsert(root)
    }

    private fun splitInsert(head: Node){
        if(head.data.length <= 1) return

        val center = head.data.length/2
        val lval = Node(head.data.substring(0, center))
        val rval = Node(head.data.substring(center))
        head.left = Node(head.data.substring(0, center))
        head.right = rval

        if(lval.data.length > 1){
            head.left?.let { left -> splitInsert(left) }
        }
        if(rval.data.length > 1){
            head.right?.let { right -> splitInsert(right) }
        }
    }

    private fun addLeaf(root: Node, list: ArrayList<String>){
        if(root.left == null && root.right == null)
            list.add(root.data)
        else{
            root.left?.let { left -> addLeaf(left, list) }
            root.right?.let { right-> addLeaf(right, list) }
        }

    }
    fun grabLeaves(): ArrayList<String>{
        val leaves = ArrayList<String>()
        addLeaf(root, leaves)
        return leaves
    }

}

fun isScramble(s1: String, s2: String): Boolean {
    if(s1.length != s2.length) return false
    return TreeString(s1).grabLeaves().sorted()
            .equals(TreeString(s2).grabLeaves().sorted())
}

// Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.
// https://leetcode.com/problems/longest-valid-parentheses/description/
fun longestValidParentheses(s: String): Int {
    TODO("not implementeed ")
}

//https://leetcode.com/problems/rotate-string/
fun rotatedString(A: String, B: String): Boolean{
    if(A.length != B.length || !B.contains(A[0])) return false

    val bDeque = ArrayDeque<Char>(B.toCharArray().toList())
    var countdown = A.length
    while(countdown > 0){
        if(bDeque.first == A[0]){
            if(A.compareTo(bDeque.joinToString("")) == 0)
                return true

            bDeque.addLast(bDeque.pop())
            countdown--
        }
        while(bDeque.first != A[0]){
            countdown--
            bDeque.addLast(bDeque.pop())
        }
    }
    return false
}

//https://leetcode.com/problems/search-a-2d-matrix/
/*
    Input:
    matrix = [
    [1,   3,  5,  7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
    ]
    target = 3
    Output: true

    Input:
    matrix = [
    [1,   3,  5,  7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
    ]
    target = 13
    Output: false
*/
fun searchMatrix(matrix: Array<IntArray>, target: Int): Boolean {

    return false
}

//https://leetcode.com/problems/longest-mountain-in-array/
/*
*
Input: [2,1,4,7,3,2,5]
Output: 5
Explanation: The largest mountain is [1,4,7,3,2] which has length 5.

Input: [2,2,2]
Output: 0
Explanation: There is no mountain.

Let's call any (contiguous) subarray B (of A) a mountain if the following properties hold:

    B.length >= 3
    There exists some 0 < i < B.length - 1 such that
        B[0] < B[1] < ... B[i-1] < B[i] > B[i+1] > ... > B[B.length - 1]


    0 <= A.length <= 10000
    0 <= A[i] <= 10000


*/


}

