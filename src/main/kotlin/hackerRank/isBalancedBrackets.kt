package hackerRank
// https://www.hackerrank.com/challenges/balanced-brackets/problem
import java.io.File
import java.lang.Exception
import java.util.*

fun isBalanced(s: String): String {
    if(s.length%2 > 0 || s.last() == '(' || s.last() == '{' || s.last() == '[') return "NO"
    val openStack = Stack<Char>()
    s.toCharArray().forEach {
        when(it){
            '{', '(', '[' -> openStack.push(it)
            '}' -> { val open = try { openStack.pop() } catch (e: Exception) {return "NO"}; if (!open.equals('{')) return "NO" }
            ')' -> { val open = try { openStack.pop() } catch (e: Exception) {return "NO"}; if(!open.equals('(')) return "NO" }
            ']' -> { val open = try { openStack.pop() } catch (e: Exception) {return "NO"}; if(!open.equals('[')) return "NO" }
        }
    }
    return "YES"
}

fun main(args: Array<String>) {
    val string1 = "{[()]}" //yes
    val string2 = "{[(])}" //no
    val string3 = "{{[[(())]]}}" //yes
    val string4 = "{{)[](}}" // no
    val string5 = "{(([])[])[]}" // yes
    val string6 = "{(([])[])[]}[]" // yes

    val string7 = "}][}}(}][))]" // no
    val string8 = "[](){()}" // yes
    val string9 = "()" // yes
    val string10 = "({}([][]))[]()" // yes
    val string11 = "{)[](}]}]}))}(())(" // no
    val string12 = "([[)" // no

    println(isBalanced(string1))
    println(isBalanced(string2))
    println(isBalanced(string3))
    println(isBalanced(string4))
    println(isBalanced(string5))
    println(isBalanced(string6))

    println(isBalanced(string7))
    println(isBalanced(string8))
    println(isBalanced(string9))
    println(isBalanced(string10))
    println(isBalanced(string11))
    println(isBalanced(string12))

//    File("resources/isBalancedBrackets.txt").forEachLine {
//        println(isBalanced(it))
//    }
}