package hackerRank
// https://www.hackerrank.com/challenges/down-to-zero-ii/problem

var minMoves = IntArray(1000001)
fun downToZero(n: Int): Int {
    if (n <= 3) return n
    if (minMoves[n] > 0) return minMoves[n]
    var min = Int.MAX_VALUE
    var i = 2
    while (i <= Math.sqrt(n.toDouble())) {
        if (n % i == 0) {
            val factor = n / i
            min = Math.min(min, 1 + downToZero(factor))
        }
        i++
    }
    min = Math.min(min, 1 + downToZero(n - 1))
    minMoves[n] = min
    return min
}

fun main(args: Array<String>) {
    // 86: 43 42 7 6 3 2 1 0 - 8 steps my solution
    println(downToZero(86))
//    java.io.File("resources/downToZero2.txt").forEachLine { println(it + " " + downToZero(it.toInt())) }

//    println(downToZero(59))
//    val scan = Scanner(System.`in`)
//    val q = scan.nextLine().trim().toInt()
//
//    for (qItr in 1..q) {
//        val n = scan.nextLine().trim().toInt()
//        val result = downToZero(n)
//        println(result)
//    }
}


//fun downToZero(n: Int): Int {
//    var countdown = n
//    var steps = 0
//
//    while(countdown != 0){
//        println(countdown)
//        val divisors = divisors(countdown)
//        val max = if(divisors.isEmpty()) 0 else Math.max(divisors.last().first, divisors.last().second)
//        if(countdown == 1 || divisors.isEmpty()){
//            countdown--
//        } else {
//            countdown = max
//        }
//        steps++
//    }
//    return steps
//}
//
//fun divisors(max: Int): ArrayList<Pair<Int,Int>> {
//    val divisors = ArrayList<Pair<Int,Int>>()
//    var maxDivisor = max / 2
//    var remainder = max % 2
//    while(maxDivisor > 1 && maxDivisor >= max/maxDivisor){
//        if(remainder == 0) divisors.add(Pair(maxDivisor, max/maxDivisor))
//        maxDivisor--
//        remainder = max % maxDivisor
//    }
//    return divisors
//}
//
//fun decrementOrDivisor(countdown: Int, divisors: ArrayList<Pair<Int,Int>>) : Int {
//    divisors.forEach {
//        return decrementOrDivisor(it.first, divisors(it.first))
//    }
//    return decrementOrDivisor(countdown-1, divisors(countdown-1))
//}