package hackerRank
// https://www.hackerrank.com/challenges/simple-text-editor/problem

import java.util.Stack

fun main(args: Array<String>) {
    val numOfInputs = readLine()!!.toInt()
    var text = ""
    val commandBuffer = Stack<String>()
    for(i in 0..numOfInputs-1){
        val line = readLine()!!
        val split = line.split(" ")
        when(split[0]){
            "1" -> { text += split[1]; commandBuffer.push(line) }
            "2" -> {
                commandBuffer.add("2 " + text.substring(text.length - split[1].toInt()))
                text = text.substring(0, text.length - split[1].toInt())
            }
            "3" -> println(text[split[1].toInt()-1])
            "4" -> {
                val command = commandBuffer.pop().split(" ")
                if(command[0].equals("1")){
                    text = text.substring(0, text.length - command[1].length)
                } else {
                    text += command[1]
                }
            }
        }
    }
}