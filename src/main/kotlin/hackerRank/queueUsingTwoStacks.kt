package hackerRank

import java.util.Stack

// https://www.hackerrank.com/challenges/queue-using-two-stacks/problem
// this passes half of the test cases. The other half execution time too long. Knew that would happen
class Queue<E>{
    val orderStack = Stack<E>()
    val queueStack = Stack<E>()

    fun enqueue(value: E){
        while(!queueStack.isEmpty()){
            orderStack.push(queueStack.pop())
        }
        orderStack.push(value)
    }

    fun dequeue(): E {
        while(!orderStack.isEmpty()){
            queueStack.push(orderStack.pop())
        }
        return queueStack.pop()
    }

    fun getHead(): E {
        while(!orderStack.isEmpty()){
            queueStack.push(orderStack.pop())
        }
        return queueStack.peek()
    }

    fun isEmpty(): Boolean {
        return (orderStack.isEmpty() and queueStack.isEmpty())
    }
}

fun main(args: Array<String>) {
    val queue = Queue<Int>()
    var lineBuffer: String
    var split: List<String>?
    val inputLength = readLine()!!.toInt()
    for(i in 0 until inputLength){
        lineBuffer = readLine()!!
        split = lineBuffer.split(" ")
        if(Integer.parseInt(split[0]) == 1){
            queue.enqueue(Integer.parseInt(split[1]))
        } else if(Integer.parseInt(split[0]) == 3) {
            println(queue.getHead())
        } else {
            queue.dequeue()
        }
    }
}