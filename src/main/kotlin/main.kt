import java.lang.NumberFormatException

fun main(args: Array<String>){
    val x = { -> {
        1;
    }}
    println(x)
//    utils.util.math.reverseNum(-12345678)
//    littleRemembers()
}

fun littleRemembers() {
    var strings = Array<String>(size = 5, init = { index -> "Item #$index" })
    var nums = Array(6) {i-> i*2};
    val arrayToSort = arrayOf(4,1,45,3,7)
    println("${arrayToSort.joinToString()} sort to -> ${utils.bubbleSort(arrayToSort).joinToString()}")

    //Remember this
    // https://medium.com/mobile-app-development-publication/kotlin-dont-just-use-let-7e91f544e27f
    var objectNull: String? = null;
    objectNull?.let {
        println("won't happen")
    } ?: run {
        println("it's null")
        objectNull = "now it's somethig"
    }
    println(objectNull)

/*
    for(i in 1..5)
    for(i in 5..1)
    for(i in 3 until 5)
    if(i in 1 until 5)
    for(i in 5 downTo 1)
    for(i in 1..5 step 2)
    for(i in 5 downTo 1 step 2)

    for (i in 5 until 7){
        println(i)
    }
    if(5 in 1..5){
        println(true)
    }
*/
    fun String.isNumber(): Boolean {
        try{
            Integer.parseInt(this)
            return true
        } catch(e: NumberFormatException) {
            return false
        }
    }
    val goodNumbers = arrayListOf<String>("1","2","3","5","6","7","8")
    val someBadNumbers = arrayListOf<String>("1","2","3","a","6","7","8")
    println("Are all numbers: $goodNumbers :  ${goodNumbers.all{it.isNumber()}}")
    println("Are all numbers: $someBadNumbers :  ${someBadNumbers.all{it.isNumber()}}")

    val concat: (String, Int) -> (String) = { str, int -> str + int }
    val joined = concat("test", 4324)
    println(joined)

    val numbers = intArrayOf(1, 2, 3, 4)
    val summation = fun (args: IntArray): Int {
        var sum = 0
        for(arg in args){
            sum += arg
        }
        return sum
    }
    fun sum(vararg args: Int): Int {
        var sum = 0
        for(arg in args){
            sum += arg
        }
        return sum
    }
    println(summation(intArrayOf(1,2,3,4,5)))
    println(sum(1,2,3,4,5))
}
