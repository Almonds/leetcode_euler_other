package dataStructures
/*Pretty sure I made something look more readable like 6 years ago in college data structures*/
class BinarySearchTree<K: Comparable<K>, V: Comparable<V>> {
    class Node<K, V>(var key: K, var value: V) {
        var left: Node<K, V>? = null
        var right: Node<K, V>? = null
    }
    var root: Node<K, V>? = null
        private set
    var size = 0
        private set

    fun add(key: K, value: V) {
        val newNode = Node<K, V>(key, value)
        if(root == null){
            root = newNode
            size++
        } else if(contains(key)){
            setValue(key, value)
        } else {
            addTraverse(newNode, root)
            size++
        }
    }

    fun setValue(key: K, value: V) {
        val updateNode = searchKey(root, key)
        updateNode?.let { it.value = value }
    }

    fun printOrdered(){
        if(root != null) {
            if(root!!.left != null) {
                printHelper(root!!.left as Node<K, V>)
            }
            println("${root!!.key}, ${root!!.value}")
            if(root!!.right != null){
                printHelper(root!!.right as Node<K,V>)
            }
        }
    }

    fun removeKey(key: K): Boolean {
        if(root == null) return false

        if(size == 1) {
            root = null
            size = 0
        } else if(root!!.key.compareTo(key) == 0) {
            removeRoot()
            size--
        } else {
            val parent = findParent(root, key)
            if(parent == null) return false
            if(parent.right?.key?.compareTo(key) == 0) {
                if(parent.right?.left == null && parent.right?.right == null){
                    parent.right = null
                } else {
                    removeNormalNodeWithChildren(parent, parent.right as Node<K,V>)
                }
            } else {
                if(parent.left?.left == null && parent.left?.right == null){
                    parent.left = null
                } else {
                    removeNormalNodeWithChildren(parent, parent.left as Node<K,V>)
                }
            }
            size--
        }
        return true
    }

    fun getValue(k: K): V? {
        return searchKey(root, k)?.value
    }

    fun contains(key: K): Boolean {
        return searchKey(root, key)?.let{true} ?: run{false}
    }

    private fun addTraverse(newNode: Node<K,V>, parent: Node<K,V>?){
        if(parent!!.key.compareTo(newNode.key) >= 0){
            if(parent.left == null){
                parent.left = newNode
                return
            }
            addTraverse(newNode, parent.left)
        } else {
            if(parent.right == null){
                parent.right = newNode
                return
            }
            addTraverse(newNode, parent.right)
        }
    }

    private fun printHelper(parent: Node<K,V>){
        if(parent.left == null && parent.right == null){
            println("${parent.key}, ${parent.value}")
            return
        }
        if(parent.left != null){
            printHelper(parent.left as Node<K,V>)
        }
        println("${parent.key}, ${parent.value}")
        if(parent.right != null) {
            printHelper((parent.right as Node<K,V>))
        }
    }

    private fun findParent(treeRoot: Node<K,V>?, key: K): Node<K,V>? {
        if(treeRoot == null) return null
        if(treeRoot.left?.key?.compareTo(key) == 0 || treeRoot.right?.key?.compareTo(key) == 0) {
            return treeRoot
        }
        if(treeRoot.key.compareTo(key) > 0) {
            return findParent(treeRoot.left, key)
        }
        return findParent(treeRoot.right, key)
    }


    private fun parentOfSubtreeSmallest(parent: Node<K,V>, subTreeRoot: Node<K,V>): Node<K,V> {
        if(subTreeRoot.left == null){
            return parent
        } else if(subTreeRoot.left?.left != null){
            return parentOfSubtreeSmallest(subTreeRoot, subTreeRoot.left as Node<K,V>)
        } else {
            return subTreeRoot
        }
    }

    private fun parentOfSubtreeLargest(parent: Node<K,V>, subTreeRoot: Node<K,V>): Node<K,V> {
        if(subTreeRoot.right == null){
            return parent
        } else if(subTreeRoot.right?.right != null){
            return parentOfSubtreeLargest(subTreeRoot, subTreeRoot.right as Node<K,V>)
        } else {
            return subTreeRoot
        }
    }

    private fun removeRoot(){
        if(root!!.right != null) {
            val newRootParent = parentOfSubtreeSmallest(root!!, root!!.right as Node<K, V>)
            if(newRootParent.left != null){
                // make smallest in right tree the root
                newRootParent.left!!.left = root!!.left
                newRootParent.left!!.right = newRootParent
                root = newRootParent.left
                newRootParent.left = null
            } else {
                // case where immediate right is the smallest of right tree
                newRootParent.left = root!!.left
                root = newRootParent
            }
        } else {
            val newRootParent = parentOfSubtreeLargest(root!!, root!!.left as Node<K, V>)
            if(newRootParent.key.compareTo(root!!.key) == 0){
                root = root?.left
            } else if(newRootParent.right != null){
                // left tree make largest the new root
                val oldRoot = root
                root = newRootParent.right
                newRootParent.right = newRootParent.right!!.left
                root!!.left = oldRoot!!.left
            } else {
                // case where immediate left is the next largest of left tree
                root = newRootParent
            }
        }
    }

    private fun removeNormalNodeWithChildren(parent: Node<K,V>, nodeToDelete: Node<K,V>) {
        if(nodeToDelete.right != null){
            val smallestRChildParent = parentOfSubtreeSmallest(nodeToDelete, nodeToDelete.right as Node<K,V>)
            if(smallestRChildParent.key.compareTo(nodeToDelete.key) == 0){
                if(parent.left?.key?.compareTo(nodeToDelete.key) == 0){
                    //immediate right, left side from parent subtree
                    parent.left = smallestRChildParent.right
                    smallestRChildParent.right?.left = nodeToDelete.left
                } else {
                    //immediate right, right side from parent subtree
                    parent.right = smallestRChildParent.right
                    smallestRChildParent.right?.left = nodeToDelete.left
                }
                nodeToDelete.left = null
                nodeToDelete.right = null
            } else {
                parent.right = smallestRChildParent.left
                smallestRChildParent.left?.right = nodeToDelete.right
                smallestRChildParent.left?.left = nodeToDelete.left
                smallestRChildParent.left = null
                nodeToDelete.left = null
                nodeToDelete.right = null
            }
            return
        } else if(nodeToDelete.left != null){
            parent.left = nodeToDelete.left
            nodeToDelete.left = null
        }
    }

    private fun searchKey(current: Node<K,V>?, findKey: K): Node<K,V>? {
        if(current == null){
            return null
        }
        if(current.key.compareTo(findKey) == 0) {
            return current
        }
        if(current.key.compareTo(findKey) > 0) {
            return searchKey(current.left, findKey)
        } else {
            return searchKey(current.right, findKey)
        }
    }
}

fun main(vararg args: String){
//    val nullOrNot = if(floor(Math.random()*100) < 50) null else "notNull"
//    println("nullOrNot = $nullOrNot")

//    val tree = BinaryTree<Int, String>()
//    tree.add(3,"Three")
//    tree.add(5,"Five")
//    tree.add(4,"Four")
//    tree.add(6,"Six")
//    tree.add(1,"One")
//    tree.add(2,"Two")
//    tree.add(0,"Zero")
//    tree.printOrdered()
//    println(tree.removeKey(3))
//    tree.printOrdered()
//    println(tree.root?.value)

//    val tree2 = BinaryTree<Int,String>()
//    tree2.add(5,"Five")
//    tree2.add(2,"Two")
//    tree2.add(4,"Four")
//    tree2.add(3,"Three")
//    tree2.removeKey(5)?.value
//    tree2.printOrdered()
//    println(tree2.root?.value)
//    println(tree2.root?.left?.value)

//    val tree3 = BinaryTree<Int,Int>()
//    tree3.add(3,3)
//    tree3.add(4,4)
//    tree3.add(1,1)
//    tree3.removeKey(3)
//    tree3.printOrdered()
//    println(tree3.root!!.value)
//    println(tree3.root!!.left!!.value)
//    println(tree3.root!!.right)

//    val tree4 = BinaryTree<Int,Int>()
//    tree4.add(4,4)
//    tree4.add(3,3)
//    tree4.add(2,2)
//    tree4.removeKey(4)
//    tree4.printOrdered()
//    println(tree4.root?.value)

//    val tree5 = BinaryTree<Int,Int>()
//    tree5.add(6,6)
//    tree5.add(1,1)
//    tree5.add(2,2)
//    tree5.add(3,3)
//    tree5.removeKey(6)
//    tree5.printOrdered()
//    println(tree5.root?.value)

//    val tree6 = BinaryTree<Int,Int>()
//    tree6.add(6,6)
//    tree6.add(1,1)
//    tree6.add(4,4)
//    tree6.add(5,5)
//    tree6.add(3,3)
//    tree6.add(2,2)
//    tree6.removeKey(6)
//    tree6.printOrdered()
//    println(tree6.root?.value)
//    println(tree6.size)
//    tree6.removeKey(2)
//    tree6.printOrdered()
//    println(tree6.size)

//    val tree7 = BinaryTree<Int,Int>()
//    tree7.add(7,7)
//    tree7.add(11,11)
//    tree7.add(15,15)
//    tree7.add(14,14)
//    tree7.add(13,13)
//    tree7.add(12,12)
//    tree7.add(9,9)
//    tree7.add(8,8)
//    tree7.add(10,10)
//    tree7.add(3,3)
//    tree7.add(4,4)
//    tree7.add(5,5)
//    tree7.add(6,6)
//    tree7.add(1,1)
//    tree7.add(0,0)
//    tree7.add(2,2)
//    tree7.removeKey(11)
//    tree7.printOrdered()

//    val tree8 = BinaryTree<Int,Int>()
//    tree8.add(10,10)
//    tree8.add(11,11)
//    tree8.add(13,13)
//    tree8.add(12,12)
//    tree8.add(14,14)
//    tree8.removeKey(11)
//    tree8.printOrdered()

    val tree9 = BinarySearchTree<Int,Int>()
    tree9.add(8,8)
    tree9.add(6,6)
    tree9.add(7,7)
    tree9.add(5,5)
    tree9.removeKey(8)
    tree9.printOrdered()
    println(tree9.root?.left?.right)
    println(tree9.root?.value)
    tree9.setValue(7, 545)
    tree9.setValue(6, 325)
    tree9.setValue(5, 123)
    println(tree9.root?.value)
    println(tree9.getValue(5))
}