package dataStructures

/*
* To be used to create LinkedList, Queue, Deque, Stack
*/
interface ListStructure<E : Comparable<E>> : Iterable<E>{
    fun addHead(item: E)
    fun addTail(item: E)
    fun size(): Int

    /*
    * Functions that return null
    * do so at failures to addIndex
    * or locate
    */
    fun removeHead(): E?
    fun removeTail(): E?
    fun addIndex(item: E, index: Int): E?
    fun removeIndex(index: Int): E?
    fun removeItem(item: E): E?
    fun getItemAtIndex(index: Int): E?

    //return -1 on failure to locate
    fun getIndexOfItem(item: E): Int

    override fun iterator(): Iterator<E>
}