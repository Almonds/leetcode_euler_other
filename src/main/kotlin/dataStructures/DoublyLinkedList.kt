package dataStructures
import java.util.NoSuchElementException

class DoublyLinkedList<E : Comparable<E>>(vararg values: E) : ListStructure<E> {
    private class Node<T>(var data : T){
        var next : Node<T>? = null
        var prev : Node<T>? = null
    }

    private var head : Node<E>? = null
    private var tail : Node<E>? = null
    private var size: Int
    init {
        size = 0
        for(elements in values){
            addTail(elements)
        }
    }

    private fun insertEmpty(item: E){
        val start = Node<E>(item)
        head = start
        tail = start
        size++
    }
    override fun addHead(item: E){
        if(size == 0){
            insertEmpty(item)
            return
        }
        val current = head
        head = Node<E>(item)
        head?.next = current
        current?.prev = head
        size++
    }

    override fun addTail(item: E){
        if(size == 0){
            insertEmpty(item)
            return
        }
        val current = tail
        tail = Node<E>(item)
        tail?.prev = current
        current?.next = tail
        size++
    }

    override fun removeHead(): E?{
        val retVal = head
        head = head?.next
        retVal?.next = null
        head?.prev = null
        size--

        return retVal?.data
    }

    override fun removeTail(): E?{
        val retVal = tail
        tail = tail?.prev
        retVal?.prev = null
        tail?.next = null
        size--

        return retVal?.data
    }

    //add from index 0(head) to size(tail)
    override fun addIndex(item: E, index: Int): E? {
        if(index > size || index < 0 ) return null
        if(size == 0){
            insertEmpty(item)
            return item
        }
        if(index == 0){
            addHead(item)
            return item
        }
        if(index == size){
            addTail(item)
            return item
        }
        var location = head
        for(i in 1..index){
            location = location?.next
        }
        val newNode = Node<E>(item)
        location?.prev?.next = newNode
        location?.prev = newNode
        newNode.next = location

        size++
        return item
    }

    override fun removeIndex(index: Int): E? {
        if(index > size || index < 0 ) return null
        if(index == 0)
            return removeHead()
        if(index == size)
            return removeTail()

        var location = head
        for(i in 1..index){
            location = location?.next
        }
        location?.prev?.next = location?.next
        location?.next?.prev = location?.prev

        size--
        return location?.data
    }

    //remove matching item that occurs closest to head
    override fun removeItem(item: E): E? {
        var current = head
        while(current != null){
            if(current.data == item)
                break
            current = current.next
        }

        if(current == null)
            return null
        current.prev?.next = current.next
        current.next?.prev = current.prev

        size--
        return  current.data
    }

    override fun getIndexOfItem(item: E): Int {
        var index = 0
        var current = head
        while(current != null){
            if(current.data.compareTo(item) == 0){
                return index
            }
            index++
            current = current.next
        }
        return -1
    }

    override fun getItemAtIndex(index: Int): E? {
        var retValue = head
        var count = 0
        while(retValue != null && count != index){
            retValue = retValue.next
            count++
        }
        return retValue?.data
    }

    fun isEmpty(): Boolean{
        return size == 0
    }

    fun sort(){
        TODO("not implemented, write sort utils")
    }

    override fun iterator(): Iterator<E> {
        return IteratorHelper(head)
    }

    override fun size() = size

    private class IteratorHelper<E>(head: Node<E>?) : Iterator<E> {
        var current : Node<E>? = head

        override fun hasNext(): Boolean {
            return current != null
        }

        override fun next(): E {
            if(hasNext()){
                val data: E = current!!.data
                current = current!!.next
                return data
            }
            else
                throw NoSuchElementException()
        }

    }
}